-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 31, 2019 at 09:01 PM
-- Server version: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.30-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `interfaces`
--

-- --------------------------------------------------------

--
-- Table structure for table `Nomina`
--

CREATE TABLE `Nomina` (
  `id_nomina` varchar(5) NOT NULL,
  `id_trabajador` varchar(15) NOT NULL,
  `fecha` int(11) NOT NULL,
  `paga` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `Producto`
--

CREATE TABLE `Producto` (
  `id` varchar(20) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `precio` float NOT NULL,
  `id_proveedor` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `producto_recibo`
--

CREATE TABLE `producto_recibo` (
  `id_recibo` varchar(20) NOT NULL,
  `id_producto` varchar(20) NOT NULL,
  `cantidad` int(10) NOT NULL,
  `precio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `Proveedor`
--

CREATE TABLE `Proveedor` (
  `cif` varchar(20) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `tlf` varchar(15) NOT NULL,
  `nombre` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `Recibo`
--

CREATE TABLE `Recibo` (
  `id` varchar(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `importe` float NOT NULL,
  `precio_total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `Stock`
--

CREATE TABLE `Stock` (
  `id` varchar(20) NOT NULL,
  `id_producto` varchar(20) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `vendidos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `Trabajadores`
--

CREATE TABLE `Trabajadores` (
  `dni` varchar(15) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `nacimiento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `telefono` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Nomina`
--
ALTER TABLE `Nomina`
  ADD PRIMARY KEY (`id_nomina`),
  ADD KEY `id_trabajador` (`id_trabajador`);

--
-- Indexes for table `Producto`
--
ALTER TABLE `Producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_proveedor` (`id_proveedor`);

--
-- Indexes for table `producto_recibo`
--
ALTER TABLE `producto_recibo`
  ADD PRIMARY KEY (`id_recibo`,`id_producto`),
  ADD KEY `id_producto` (`id_producto`);

--
-- Indexes for table `Proveedor`
--
ALTER TABLE `Proveedor`
  ADD PRIMARY KEY (`cif`);

--
-- Indexes for table `Recibo`
--
ALTER TABLE `Recibo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Stock`
--
ALTER TABLE `Stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_producto` (`id_producto`);

--
-- Indexes for table `Trabajadores`
--
ALTER TABLE `Trabajadores`
  ADD PRIMARY KEY (`dni`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Nomina`
--
ALTER TABLE `Nomina`
  ADD CONSTRAINT `Nomina_ibfk_1` FOREIGN KEY (`id_trabajador`) REFERENCES `Trabajadores` (`dni`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `Producto`
--
ALTER TABLE `Producto`
  ADD CONSTRAINT `Producto_ibfk_1` FOREIGN KEY (`id_proveedor`) REFERENCES `Proveedor` (`cif`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `producto_recibo`
--
ALTER TABLE `producto_recibo`
  ADD CONSTRAINT `producto_recibo_ibfk_1` FOREIGN KEY (`id_recibo`) REFERENCES `Recibo` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `producto_recibo_ibfk_2` FOREIGN KEY (`id_producto`) REFERENCES `Producto` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `Stock`
--
ALTER TABLE `Stock`
  ADD CONSTRAINT `Stock_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `Producto` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
