package com.interfaces.tienda.swing;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.interfaces.tienda.datos.daos.NominaDAO;
import com.interfaces.tienda.datos.daos.ProductoDAO;
import com.interfaces.tienda.datos.daos.ProveedorDAO;
import com.interfaces.tienda.datos.daos.StockDAO;
import com.interfaces.tienda.datos.daos.TrabajadorDAO;
import com.interfaces.tienda.modelo.Nomina;
import com.interfaces.tienda.modelo.Trabajador;
import com.interfaces.tienda.swing.crud.Crud;
import com.interfaces.tienda.swing.crud.PestanaCrud;
//Jfrane creado por juvera
public class MyJFrame extends JFrame{
	
	
	public enum Modulos{
		
		VENTA, NOMINA, PRODUCTO,PROVEEDOR, RECIBO, STOCK, TRABAJADOR
	}
	
	private int ancho;
	private int alto;
	private int x;
	private int y;
	
	public MyJFrame() {
		
		ancho = 800;
		alto = 600;
		
		x = 200;
		y = 200;
		
		setTitle("Tienda");
		
		setBounds(x,y,ancho,alto);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//add(new Login());
		
		JTabbedPane pestanas=new JTabbedPane();
		
		 //Creamos el panel y lo a�adimos a las pesta�as
        JPanel panel1=new JPanel();
 
        //Componentes del panel1
        JLabel et_p1=new JLabel("Estas en el panel 1");
        panel1.add(et_p1);

 
        //A�adimos un nombre de la pesta�a y el panel
        //pesta�as.addTab("Venta", new VentaPanel());
        pestanas.addTab("Venta", new VentaPanel());
        pestanas.addTab("Trabajadores", new PestanaCrud(this, new Crud(new TrabajadorDAO(), Headers.columnNamesTrabajador), Modulos.TRABAJADOR));
        pestanas.addTab("Nominas", new PestanaCrud(this, new Crud(new NominaDAO(), Headers.columnNamesNomina), Modulos.NOMINA));
        pestanas.addTab("Productos", new PestanaCrud(this, new Crud(new ProductoDAO(), Headers.columnNamesPreoducto), Modulos.PRODUCTO));
        pestanas.addTab("Stock", new PestanaCrud(this, new Crud(new StockDAO(), Headers.columnNamesStock), Modulos.STOCK));
        pestanas.addTab("Proveedores", new PestanaCrud(this, new Crud(new ProveedorDAO(), Headers.columnNamesProveedor), Modulos.PROVEEDOR));
        
      
        getContentPane().add(pestanas);
        
       
        setVisible(true);
        
		
	}
	
	public void desactivarFrame() {
		
		this.setEnabled(false);
	}
	
	public void activarFrame() {
		
		this.setEnabled(true);
		
	}
	
	
	
}
