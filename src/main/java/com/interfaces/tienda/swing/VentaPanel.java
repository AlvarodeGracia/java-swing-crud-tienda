package com.interfaces.tienda.swing;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.xml.crypto.Data;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.daos.ProductoDAO;
import com.interfaces.tienda.modelo.Producto;
import com.interfaces.tienda.modelo.TableData;

public class VentaPanel extends JPanel  implements ActionListener{
	
	private JPanel cabecera, pie;
	
	private JTextField textIdProducto;
	private JLabel labelIdProducto;
	private JButton btn_siguiente;
	private JButton btn_eliminar;
	private JButton btn_refresh;
	
	private JButton realizar_compra;
	
	protected String[] columnNames = {"Seleccionado","ID","Nombre", "Cantidad", "Precio", "Total"};
	protected Object[][] data;

	protected DAOInterface dao;

	protected JTable table;
	protected JScrollPane scrollPane;

	protected ArrayList<String> idsChanged;

	public VentaPanel() {
		
		this.dao = null;
		
		cabecera = new JPanel();
		pie = new JPanel();

		idsChanged = new ArrayList<String>();
		
		data = new Data[0][columnNames.length];

		generateTable();
		
		textIdProducto = new JTextField();
		textIdProducto.setSize(100, 20);
		textIdProducto.setMinimumSize(new Dimension(100, 20));
		textIdProducto.setPreferredSize(new Dimension(100, 20));
		
		btn_siguiente = new JButton("Next");
		btn_siguiente.addActionListener(this);
		
		labelIdProducto = new JLabel("ID Producto");
		labelIdProducto.setSize(100, 20);
		
		btn_eliminar = new JButton("Borrar");
		btn_eliminar.addActionListener(this);

		btn_refresh = new JButton("Refrescar");
		btn_refresh.addActionListener(this);
		
		cabecera = new JPanel();
		cabecera.add(textIdProducto);
		cabecera.add(labelIdProducto);
		cabecera.add(btn_siguiente);
		cabecera.add(btn_eliminar);
		cabecera.add(btn_refresh);

		
		realizar_compra = new JButton("Terminar");
		realizar_compra.addActionListener(this);
		
		pie.add(realizar_compra);
		
		this.add(cabecera);
		this.add(scrollPane);
		this.add(pie);
		

	}

	protected void generateTable() {

		DefaultTableModel dtm = new DefaultTableModel(data, columnNames);

		table = new JTable(dtm);

		table.getModel().addTableModelListener(new TableModelListener() {

			public void tableChanged(TableModelEvent e) {

				if (table.getRowCount() > 0) {

					try {
						
						String id = (String) table.getModel().getValueAt(e.getLastRow(), 1);
						int row = getRowByValue(id);
						if (table.getRowCount() > 0)
							idsChanged.add(id);
						
					}catch(ArrayIndexOutOfBoundsException exception) {
						
					}
					
				}

			}
		});

		// Poner checkbox en la columna index 0
		TableColumn tc = table.getColumnModel().getColumn(0);
		tc.setCellEditor(table.getDefaultEditor(Boolean.class));
		tc.setCellRenderer(table.getDefaultRenderer(Boolean.class));

		scrollPane = new JScrollPane(table);

	}

	public JScrollPane getCrud() {

		return scrollPane;
	}

	public JTable getTable() {
		return table;
	}

	public void deleteRow() {

		for (int i = table.getModel().getRowCount() - 1; i >= 0; --i) {

			if (table.getModel().getValueAt(i, 0).equals(true)) {

				String id = (String) table.getModel().getValueAt(i, 1);
				int row = getRowByValue(id);
				((DefaultTableModel) table.getModel()).removeRow(row);

			}
		}

	}

	public int getRowByValue(Object id) {

		for (int i = table.getModel().getRowCount() - 1; i >= 0; --i) {

			if (table.getModel().getValueAt(i, 1).equals(id)) {

				return i;

			}
		}

		return -1;
	}

	public void saveRowsChanged() {

		if(idsChanged.size()>0) {
			for (String id : idsChanged) {

				System.out.println("Se busca el id: " + id);
				int row = getRowByValue(id);
				if (row != -1) {

					
					table.getModel().setValueAt(String.valueOf(Float.parseFloat((String) table.getModel().getValueAt(row, 3)) *  Float.parseFloat((String) table.getModel().getValueAt(row, 4))),  row, 5);
					
				}

			}

			System.out.println("Se han actualizado: " + idsChanged.size());
			idsChanged = new ArrayList<String>();
		}
		

	}

	public void updateTable() {

		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		tableModel.setRowCount(0);
		List<TableData> object = dao.findAll();
		Object[] rowData = new Object[columnNames.length + 1];
		for (int i = 0; i < object.size(); i++) {
			tableModel.addRow(object.get(i).getData());
		}
		table.setModel(tableModel);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		JButton boton = (JButton) e.getSource();
		
		if(boton == btn_siguiente) {
			
			DAOInterface dao = new ProductoDAO();
			Producto p = (Producto) dao.findById(textIdProducto.getText());
			
			if(p != null) {
				
				DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
				tableModel.setRowCount(0);
				List<TableData> object = dao.findAll();
				
				Object[] rowData = new Object[columnNames.length];
				rowData[0] = false;
				rowData[1] = p.getId();
				rowData[2] = p.getNombre();
				rowData[3] = 1;
				rowData[4] = p.getPrecio();
				rowData[5] = p.getPrecio();
				
				tableModel.addRow(rowData);
				
				table.setModel(tableModel);
				
			}
			
			
		}else if(boton == btn_eliminar) {
			deleteRow();
			
		}else if(boton == btn_refresh) {
			
			saveRowsChanged();
			
		}else if(boton == realizar_compra) {
			
			DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
			tableModel.setRowCount(0);
			table.setModel(tableModel);
			
		}
		
		
	}

}
