package com.interfaces.tienda.swing.inserts;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.daos.TrabajadorDAO;
import com.interfaces.tienda.modelo.Trabajador;
import com.interfaces.tienda.swing.MyJFrame;

public class TrabajadorInsert extends JFrame implements ActionListener{

	private MyJFrame myframe;
	private JPanel panel;
	
	private JLabel labelDni;
	private JTextField textDni;
	
	private JLabel labelNombre;
	private JTextField textNombre;
	
	private JLabel labelApellidos;
	private JTextField textApellidos;
	
	private JLabel labelDate;
	private JTextField textDate;
	
	private JLabel labelTlf;
	private JTextField textTlf;
	
	private JButton insertar;
	private JButton cancelar;
	
	private int ancho, alto, x, y;
	
	private DAOInterface<Trabajador, String> dao;
	
	public TrabajadorInsert(MyJFrame myframe) {
		
		ancho = 500;
		alto = 500;
		
		x = 200;
		y = 200;
		
		dao = new TrabajadorDAO();
		
		setTitle("Tienda");
		setBounds(x,y,ancho,alto);
		
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		this.myframe = myframe;
		
		labelDni = new JLabel("DNI: ");
		labelDni.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textDni = new JTextField();
		textDni.setPreferredSize(new Dimension(100, 20));
		textDni.setMaximumSize(new Dimension(100, 20));
		textDni.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		labelNombre = new JLabel("Nombre: ");
		labelNombre.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textNombre = new JTextField();
		textNombre.setPreferredSize(new Dimension(100, 20));
		textNombre.setMaximumSize(new Dimension(100, 20));
		textNombre.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		labelApellidos = new JLabel("Apellidos: ");
		labelApellidos.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textApellidos = new JTextField();
		textApellidos.setPreferredSize(new Dimension(100, 20));
		textApellidos.setMaximumSize(new Dimension(100, 20));
		textApellidos.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		labelDate = new JLabel("Date: ");
		labelDate.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textDate = new JTextField();
		textDate.setPreferredSize(new Dimension(100, 20));
		textDate.setMaximumSize(new Dimension(100, 20));
		textDate.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		labelTlf = new JLabel("Telefonos: ");
		labelTlf.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textTlf = new JTextField();
		textTlf.setPreferredSize(new Dimension(100, 20));
		textTlf.setMaximumSize(new Dimension(100, 20));
		textTlf.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		panel = new JPanel();
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		panel.add(labelDni);
		panel.add(textDni);
		
		panel.add(labelNombre);
		panel.add(textNombre);
		
		panel.add(labelApellidos);
		panel.add(textApellidos);
		
		panel.add(labelDate);
		panel.add(textDate);
		
		panel.add(labelTlf);
		panel.add(textTlf);
		
		insertar = new JButton("Insertar");
		panel.add(insertar);
		insertar.addActionListener(this);
		insertar.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		add(panel);
		
		cancelar = new JButton("Cancelar");
		panel.add(cancelar);
		cancelar.addActionListener(this);
		cancelar.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton)e.getSource();
		
		if(boton == insertar) {
			
			Trabajador t = dao.findById(textDni.getText());
			if(t != null) {
				
				JOptionPane.showMessageDialog(null, "El ID ya esta en la Base de datos","Error BBDD", JOptionPane.ERROR_MESSAGE);
				
				System.out.println("Ya esta en la BBDD");
				
			}else {
				Trabajador trabajador = new Trabajador();
				
				
				try {
					
					trabajador.setDni(textDni.getText());
					trabajador.setNombre(textNombre.getText());
					trabajador.setApellidos(textApellidos.getText());
					trabajador.setNacimiento(new SimpleDateFormat("yyyy-MM-dd").parse(textDate.getText()));
					trabajador.setTlf(textTlf.getText());
					
					dao.insert(trabajador);
					
					myframe.activarFrame();
					
					this.dispose();
					
				} catch (ParseException e1) {
					JOptionPane.showMessageDialog(null, "Formato Fecha Erroneo: yyyy-MM-dd","Error BBDD", JOptionPane.ERROR_MESSAGE);
				}
				
				
				
				
			}
			
			
		}else if(boton == cancelar) {
			myframe.activarFrame();
			this.dispose();
		}
		
	}
	
	
}
