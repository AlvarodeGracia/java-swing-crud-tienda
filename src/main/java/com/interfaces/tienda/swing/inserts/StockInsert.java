package com.interfaces.tienda.swing.inserts;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.daos.ProductoDAO;
import com.interfaces.tienda.datos.daos.StockDAO;
import com.interfaces.tienda.modelo.Stock;
import com.interfaces.tienda.swing.MyJFrame;

public class StockInsert extends JFrame implements ActionListener{

	private MyJFrame myframe;
	private JPanel panel;
	
	private JLabel labelId;
	private JTextField textId;
	
	private JLabel labelProducto;
	private JTextField textProducto;
	
	private JLabel labelCantidad;
	private JTextField textCantidad;
	
	private JButton insertar;
	private JButton cancelar;
	
	private int ancho, alto, x, y;
	
	private DAOInterface<Stock, String> dao;
	
	public StockInsert (MyJFrame myframe) {
		
		ancho = 500;
		alto = 500;
		
		x = 200;
		y = 200;
		
		dao = new StockDAO();
		
		setTitle("Tienda");
		setBounds(x,y,ancho,alto);
		
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		this.myframe = myframe;
		
		labelId = new JLabel("Identificador: ");
		labelId.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textId = new JTextField();
		textId.setPreferredSize(new Dimension(100, 20));
		textId.setMaximumSize(new Dimension(100, 20));
		textId.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		labelProducto = new JLabel("idProducto: ");
		labelProducto.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textProducto = new JTextField();
		textProducto.setPreferredSize(new Dimension(100, 20));
		textProducto.setMaximumSize(new Dimension(100, 20));
		textProducto.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		labelCantidad = new JLabel("Cantidad: ");
		labelCantidad.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textCantidad = new JTextField();
		textCantidad.setPreferredSize(new Dimension(100, 20));
		textCantidad.setMaximumSize(new Dimension(100, 20));
		textCantidad.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		panel = new JPanel();
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		panel.add(labelId);
		panel.add(textId);
		
		panel.add(labelProducto);
		panel.add(textProducto);
		
		panel.add(labelCantidad);
		panel.add(textCantidad);
		
		insertar = new JButton("Insertar");
		panel.add(insertar);
		insertar.addActionListener(this);
		insertar.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		add(panel);
		
		cancelar = new JButton("Cancelar");
		panel.add(cancelar);
		cancelar.addActionListener(this);
		cancelar.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton)e.getSource();
		
		if(boton == insertar) {
			
			Stock t = dao.findById(textId.getText());
			if(t != null) {
				
				JOptionPane.showMessageDialog(null, "El ID ya esta en la Base de datos","Error BBDD", JOptionPane.ERROR_MESSAGE);
				
				System.out.println("Ya esta en la BBDD");
				
			}else {
				Stock stock = new Stock();
				
				
				stock.setId(textId.getText());
				stock.setProducto(new ProductoDAO().findById(textProducto.getText()));
				stock.setCantidad(Integer.parseInt(textCantidad.getText()));
				
				dao.insert(stock);
				
				myframe.activarFrame();
				
				this.dispose();
				
				
			}
			
			
		}else if(boton == cancelar) {
			myframe.activarFrame();
			this.dispose();
		}
		
	}
	
}
