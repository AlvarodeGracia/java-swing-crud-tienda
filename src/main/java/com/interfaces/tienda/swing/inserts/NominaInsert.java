package com.interfaces.tienda.swing.inserts;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.daos.NominaDAO;
import com.interfaces.tienda.datos.daos.TrabajadorDAO;
import com.interfaces.tienda.modelo.Nomina;
import com.interfaces.tienda.modelo.Trabajador;
import com.interfaces.tienda.swing.MyJFrame;

public class NominaInsert extends JFrame implements ActionListener {

	private MyJFrame myframe;
	private JPanel panel;

	private JLabel labelId;
	private JTextField textId;

	private JLabel labelTrabajador;
	private JTextField textTrabajador;

	private JLabel labelDate;
	private JTextField textDate;

	private JLabel labelPaga;
	private JTextField textPaga;

	private JButton insertar;
	private JButton cancelar;

	private int ancho, alto, x, y;

	private DAOInterface<Trabajador, String> daoTrabajador;
	private DAOInterface<Nomina, String> daoNomina;

	public NominaInsert(MyJFrame myframe) {

		ancho = 500;
		alto = 500;

		x = 200;
		y = 200;

		daoTrabajador = new TrabajadorDAO();
		daoNomina = new NominaDAO();

		setTitle("Tienda");
		setBounds(x, y, ancho, alto);

		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		this.myframe = myframe;

		labelId = new JLabel("ID:");
		labelId.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textId = new JTextField();
		textId.setPreferredSize(new Dimension(100, 20));
		textId.setMaximumSize(new Dimension(100, 20));
		textId.setAlignmentX(JComponent.CENTER_ALIGNMENT);

		labelTrabajador = new JLabel("DNI Trabajador: ");
		labelTrabajador.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textTrabajador = new JTextField();
		textTrabajador.setPreferredSize(new Dimension(100, 20));
		textTrabajador.setMaximumSize(new Dimension(100, 20));
		textTrabajador.setAlignmentX(JComponent.CENTER_ALIGNMENT);

		labelDate = new JLabel("Date: ");
		labelDate.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textDate = new JTextField();
		textDate.setPreferredSize(new Dimension(100, 20));
		textDate.setMaximumSize(new Dimension(100, 20));
		textDate.setAlignmentX(JComponent.CENTER_ALIGNMENT);

		labelPaga = new JLabel("Paga: ");
		labelPaga.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textPaga = new JTextField();
		textPaga.setPreferredSize(new Dimension(100, 20));
		textPaga.setMaximumSize(new Dimension(100, 20));
		textPaga.setAlignmentX(JComponent.CENTER_ALIGNMENT);

		panel = new JPanel();

		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		panel.add(labelId);
		panel.add(textId);

		panel.add(labelTrabajador);
		panel.add(textTrabajador);

		panel.add(labelDate);
		panel.add(textDate);

		panel.add(labelPaga);
		panel.add(textPaga);

		insertar = new JButton("Insertar");
		panel.add(insertar);
		insertar.addActionListener(this);
		insertar.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		add(panel);

		cancelar = new JButton("Cancelar");
		panel.add(cancelar);
		cancelar.addActionListener(this);
		cancelar.setAlignmentX(JComponent.CENTER_ALIGNMENT);

		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton) e.getSource();

		if (boton == insertar) {

			Nomina n = daoNomina.findById(textId.getText());

			if (n != null) {

				JOptionPane.showMessageDialog(null, "Ese ID ya esta en la BBDD", "Error BBDD",
						JOptionPane.ERROR_MESSAGE);

				System.out.println("Ya esta en la BBDD");

			} else {

				Trabajador t = daoTrabajador.findById(textTrabajador.getText());
				if (t == null) {

					JOptionPane.showMessageDialog(null, "Ese Trabajador No esta", "Error BBDD",
							JOptionPane.ERROR_MESSAGE);

					System.out.println("Ya esta en la BBDD");

				} else {

					try {
						Nomina nomina = new Nomina();
						nomina.setId(textId.getText());
						nomina.setTrabajador(t);
						nomina.setFecha(new SimpleDateFormat("yyyy-MM-dd").parse(textDate.getText()));
						nomina.setPaga(Float.parseFloat(textPaga.getText()));

						daoNomina.insert(nomina);

						myframe.activarFrame();

						this.dispose();

					} catch (ParseException e1) {
						JOptionPane.showMessageDialog(null, "Formato Fecha Erroneo: yyyy-MM-dd", "Error BBDD",
								JOptionPane.ERROR_MESSAGE);
					}

				}

			}

		} else if (boton == cancelar)

		{
			myframe.activarFrame();
			this.dispose();
		}

	}

}
