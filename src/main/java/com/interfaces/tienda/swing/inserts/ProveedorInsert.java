package com.interfaces.tienda.swing.inserts;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.daos.ProveedorDAO;
import com.interfaces.tienda.modelo.Proveedor;
import com.interfaces.tienda.swing.MyJFrame;

public class ProveedorInsert extends JFrame implements ActionListener{

	private MyJFrame myframe;
	private JPanel panel;
	
	private JLabel labelCif;
	private JTextField textCif;
	
	private JLabel labelNombre;
	private JTextField textNombre;
	
	private JLabel labelDireccion;
	private JTextField textDireccion;
	
	private JLabel labelTlf;
	private JTextField textTlf;
	
	private JButton insertar;
	private JButton cancelar;
	
	private int ancho, alto, x, y;
	
	private DAOInterface<Proveedor, String> dao;
	
	public ProveedorInsert(MyJFrame myframe) {
		
		ancho = 500;
		alto = 500;
		
		x = 200;
		y = 200;
		
		dao = new ProveedorDAO();
		
		setTitle("Tienda");
		setBounds(x,y,ancho,alto);
		
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		this.myframe = myframe;
		
		labelCif = new JLabel("Cif: ");
		labelCif.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textCif = new JTextField();
		textCif.setPreferredSize(new Dimension(100, 20));
		textCif.setMaximumSize(new Dimension(100, 20));
		textCif.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		labelNombre = new JLabel("Nombre: ");
		labelNombre.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textNombre = new JTextField();
		textNombre.setPreferredSize(new Dimension(100, 20));
		textNombre.setMaximumSize(new Dimension(100, 20));
		textNombre.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		labelDireccion = new JLabel("Direccion: ");
		labelDireccion.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textDireccion = new JTextField();
		textDireccion.setPreferredSize(new Dimension(100, 20));
		textDireccion.setMaximumSize(new Dimension(100, 20));
		textDireccion.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		labelTlf = new JLabel("Telefonos: ");
		labelTlf.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textTlf = new JTextField();
		textTlf.setPreferredSize(new Dimension(100, 20));
		textTlf.setMaximumSize(new Dimension(100, 20));
		textTlf.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		panel = new JPanel();
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		panel.add(labelCif);
		panel.add(textCif);
		
		panel.add(labelNombre);
		panel.add(textNombre);
		
		panel.add(labelDireccion);
		panel.add(textDireccion);
		
		panel.add(labelTlf);
		panel.add(textTlf);
		
		insertar = new JButton("Insertar");
		panel.add(insertar);
		insertar.addActionListener(this);
		insertar.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		add(panel);
		
		cancelar = new JButton("Cancelar");
		panel.add(cancelar);
		cancelar.addActionListener(this);
		cancelar.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton)e.getSource();
		
		if(boton == insertar) {
			
			Proveedor t = dao.findById(textCif.getText());
			if(t != null) {
				
				JOptionPane.showMessageDialog(null, "El ID ya esta en la Base de datos","Error BBDD", JOptionPane.ERROR_MESSAGE);
				
				System.out.println("Ya esta en la BBDD");
				
			}else {
				Proveedor proveedor = new Proveedor();
				
				
				proveedor.setCif(textCif.getText());
				proveedor.setNombre(textNombre.getText());
				proveedor.setDireccion(textDireccion.getText());
				proveedor.setTlf(textTlf.getText());
				
				dao.insert(proveedor);
				
				myframe.activarFrame();
				
				this.dispose();
				
				
			}
			
			
		}else if(boton == cancelar) {
			myframe.activarFrame();
			this.dispose();
		}
		
	}
	
}
