package com.interfaces.tienda.swing.inserts;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.daos.ProductoDAO;
import com.interfaces.tienda.datos.daos.ProveedorDAO;
import com.interfaces.tienda.datos.daos.ProductoDAO;
import com.interfaces.tienda.modelo.Producto;
import com.interfaces.tienda.modelo.Producto;
import com.interfaces.tienda.swing.MyJFrame;

public class ProductoInsert extends JFrame implements ActionListener{

	private MyJFrame myframe;
	private JPanel panel;
	
	private JLabel labelId;
	private JTextField textId;
	
	private JLabel labelNombre;
	private JTextField textNombre;
	
	private JLabel labelProducto;
	private JTextField textProducto;
	
	private JLabel labelPrecio;
	private JTextField textPrecio;
	
	private JButton insertar;
	private JButton cancelar;
	
	private int ancho, alto, x, y;
	
	private DAOInterface<Producto, String> dao;
	
	public ProductoInsert(MyJFrame myframe) {
		
		ancho = 500;
		alto = 500;
		
		x = 200;
		y = 200;
		
		dao = new ProductoDAO();
		
		setTitle("Tienda");
		setBounds(x,y,ancho,alto);
		
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		this.myframe = myframe;
		
		labelId = new JLabel("Identificador: ");
		labelId.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textId = new JTextField();
		textId.setPreferredSize(new Dimension(100, 20));
		textId.setMaximumSize(new Dimension(100, 20));
		textId.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		labelNombre = new JLabel("Nombre: ");
		labelNombre.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textNombre = new JTextField();
		textNombre.setPreferredSize(new Dimension(100, 20));
		textNombre.setMaximumSize(new Dimension(100, 20));
		textNombre.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		labelProducto = new JLabel("Proveedor: ");
		labelProducto.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textProducto = new JTextField();
		textProducto.setPreferredSize(new Dimension(100, 20));
		textProducto.setMaximumSize(new Dimension(100, 20));
		textProducto.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		labelPrecio = new JLabel("Precio: ");
		labelPrecio.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		textPrecio = new JTextField();
		textPrecio.setPreferredSize(new Dimension(100, 20));
		textPrecio.setMaximumSize(new Dimension(100, 20));
		textPrecio.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		panel = new JPanel();
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		panel.add(labelId);
		panel.add(textId);
		
		panel.add(labelNombre);
		panel.add(textNombre);
		
		panel.add(labelProducto);
		panel.add(textProducto);
		
		panel.add(labelPrecio);
		panel.add(textPrecio);
		
		insertar = new JButton("Insertar");
		panel.add(insertar);
		insertar.addActionListener(this);
		insertar.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		add(panel);
		
		cancelar = new JButton("Cancelar");
		panel.add(cancelar);
		cancelar.addActionListener(this);
		cancelar.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		
		
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton boton = (JButton)e.getSource();
		
		if(boton == insertar) {
			
			Producto t = dao.findById(textId.getText());
			if(t != null) {
				
				JOptionPane.showMessageDialog(null, "El ID ya esta en la Base de datos","Error BBDD", JOptionPane.ERROR_MESSAGE);
				
				System.out.println("Ya esta en la BBDD");
				
			}else {
				Producto producto = new Producto();
				
				
				producto.setId(textId.getText());
				producto.setNombre(textNombre.getText());
				producto.setProveedor(new ProveedorDAO().findById(textProducto.getText()));
				producto.setPrecio(Integer.parseInt(textPrecio.getText()));
				
				dao.insert(producto);
				
				myframe.activarFrame();
				
				this.dispose();
				
				
			}
			
			
		}else if(boton == cancelar) {
			myframe.activarFrame();
			this.dispose();
		}
		
	}
	
}
