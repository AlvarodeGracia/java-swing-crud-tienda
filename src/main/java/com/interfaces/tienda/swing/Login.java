package com.interfaces.tienda.swing;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.daos.TrabajadorDAO;
import com.interfaces.tienda.modelo.Trabajador;

public class Login extends JPanel implements ActionListener{

	private JTextField user;
	private JPasswordField pass;
	private JLabel label1;
	private JLabel label2;
	private JButton boton;
	private final String passwd = "1234";
	
	private MyJFrame myFrame;
	
	public Login (MyJFrame myFrame) {
		
		this.myFrame = myFrame;
		
		//setLayout(new GridLayout(0,1));
		
		user = new JTextField("",10);
		pass = new JPasswordField("",10);
		label1 = new JLabel("Usuario: ");
		label2 = new JLabel("Contrasenia: ");
		boton = new JButton("Enviar");
		boton.addActionListener(this);
		
		JPanel panel1 = new JPanel();
		panel1.add(label1);
		panel1.add(user);
		
		JPanel panel2 = new JPanel();
		panel2.add(label2);
		panel2.add(pass);
		
		JPanel panel3 = new JPanel();
		panel3.add(boton);
		
		JPanel panel4 = new JPanel();
		panel4.setLayout(new GridLayout(0,1));
		panel4.add(panel1);
		
		panel4.add(panel2);
		panel4.add(panel3);
		
		add(panel4);
		
		
	}

	public void actionPerformed(ActionEvent e) {
		 
		JButton evento = (JButton) e.getSource();
		
		if (evento == boton) {
			
			String usuario = user.getText();
			String contra = new String(pass.getPassword());
			System.out.println(contra);
			
			if (contra.equals(passwd)) {
				
				DAOInterface dao = new TrabajadorDAO();
				
				if (dao.findById(usuario) != null)
					JOptionPane.showMessageDialog(null, "Acceso concedido", "Acceso", JOptionPane.OK_OPTION);
					
				else
					JOptionPane.showMessageDialog(null, "Usuario incorrecto", "Error", JOptionPane.ERROR_MESSAGE);
				
			}
			else {
				JOptionPane.showMessageDialog(null, "Password incorrecta", "Error", JOptionPane.ERROR_MESSAGE);
			}
			
		}
		
	}
	
	
}
