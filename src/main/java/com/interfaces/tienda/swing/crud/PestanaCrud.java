package com.interfaces.tienda.swing.crud;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.interfaces.tienda.datos.daos.TrabajadorDAO;
import com.interfaces.tienda.swing.MyJFrame;
import com.interfaces.tienda.swing.MyJFrame.Modulos;
import com.interfaces.tienda.swing.inserts.NominaInsert;
import com.interfaces.tienda.swing.inserts.ProductoInsert;
import com.interfaces.tienda.swing.inserts.ProveedorInsert;
import com.interfaces.tienda.swing.inserts.StockInsert;
import com.interfaces.tienda.swing.inserts.TrabajadorInsert;

public class PestanaCrud extends JPanel implements ActionListener {

	private JTextField campoBusqueda;
	private JButton btn_buscar;
	private JButton btn_insertar;
	private JButton btn_update;
	private JButton btn_eliminar;
	private JButton btn_refresh;
	private JPanel cabecera;

	private Crud crud;
	
	private MyJFrame myJFrame;
	
	private Modulos modulo;

	public PestanaCrud(MyJFrame myJFrame, Crud crud, Modulos modulo) {
		
		this.modulo = modulo;
		
		this.crud = crud;
		this.myJFrame = myJFrame;
		campoBusqueda = new JTextField();
		campoBusqueda.setSize(100, 20);
		campoBusqueda.setMinimumSize(new Dimension(100, 20));
		campoBusqueda.setPreferredSize(new Dimension(100, 20));
		btn_buscar = new JButton("Buscar");
		btn_buscar.addActionListener(this);
		btn_insertar = new JButton("Insertar");
		btn_insertar.addActionListener(this);
		btn_update = new JButton("Actualizar");
		btn_update.addActionListener(this);
		btn_eliminar = new JButton("Borrar");
		btn_eliminar.addActionListener(this);

		btn_refresh = new JButton("Refrescar");
		btn_refresh.addActionListener(this);
		
		cabecera = new JPanel();
		cabecera.add(campoBusqueda);
		cabecera.add(btn_buscar);
		cabecera.add(btn_insertar);
		cabecera.add(btn_update);
		cabecera.add(btn_eliminar);
		cabecera.add(btn_refresh);

		this.add(cabecera);
		this.add(crud.getCrud());

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		JButton boton = (JButton) e.getSource();

		if (btn_buscar == boton) {

			System.out.println("Busca r id " + campoBusqueda.getText());
			crud.updateTable(new String(campoBusqueda.getText()));
			

		} else if (btn_eliminar == boton) {

			crud.deleteRow();
		}else if(btn_update == boton) {
			crud.saveRowsChanged();
		}else if(btn_insertar == boton) {
			
			switch(modulo) {
			case NOMINA:
				NominaInsert frameInsert = new NominaInsert(myJFrame);
				myJFrame.desactivarFrame();
				break;
			case PRODUCTO:
				ProductoInsert productoInsert = new ProductoInsert(myJFrame);
				myJFrame.desactivarFrame();
				break;
			case PROVEEDOR:
				ProveedorInsert proveedorInsert = new ProveedorInsert(myJFrame);
				myJFrame.desactivarFrame();
				break;
			case RECIBO:
				break;
			case STOCK:
				StockInsert stockInsert = new StockInsert(myJFrame);
				myJFrame.desactivarFrame();
				break;
			case TRABAJADOR:
				TrabajadorInsert frameIsnert= new TrabajadorInsert(myJFrame);
				myJFrame.desactivarFrame();
				break;
			case VENTA:
				break;
			default:
				break;
			
			}
			
			
			
			
		}else if(btn_refresh == boton) {
			
			crud.updateTable(null);
			
		}

	}

	

	

}
