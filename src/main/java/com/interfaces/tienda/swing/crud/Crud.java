package com.interfaces.tienda.swing.crud;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.modelo.TableData;

public class Crud {

	protected String[] columnNames;
	protected Object[][] data;

	protected DAOInterface dao;

	protected JTable table;

	protected JScrollPane scrollPane;

	protected ArrayList<String> idsChanged;

	public Crud(DAOInterface dao, String[] columnNames) {
		
		this.dao = dao;

		idsChanged = new ArrayList<String>();
		
		List<TableData> objects = dao.findAll();
		
		this.columnNames = columnNames;
		
		if(objects.size() > 0) {
			
			data = new Object[objects.size()][this.columnNames.length];

			for (int i = 0; i < objects.size(); i++) {
				data[i] = objects.get(i).getData();
			}

		}
		
		generateTable();

	}

	protected void generateTable() {

		DefaultTableModel dtm = new DefaultTableModel(data, columnNames);

		table = new JTable(dtm);

		table.getModel().addTableModelListener(new TableModelListener() {

			public void tableChanged(TableModelEvent e) {

				if (table.getRowCount() > 0) {

					try {
						
						String id = (String) table.getModel().getValueAt(e.getLastRow(), 1);
						int row = getRowByValue(id);
						if (table.getRowCount() > 0)
							idsChanged.add(id);
						
					}catch(ArrayIndexOutOfBoundsException exception) {
						
					}
					
				}

			}
		});

		// Poner checkbox en la columna index 0
		TableColumn tc = table.getColumnModel().getColumn(0);
		tc.setCellEditor(table.getDefaultEditor(Boolean.class));
		tc.setCellRenderer(table.getDefaultRenderer(Boolean.class));

		scrollPane = new JScrollPane(table);

	}

	public JScrollPane getCrud() {

		return scrollPane;
	}

	public JTable getTable() {
		return table;
	}

	public void deleteRow() {

		for (int i = table.getModel().getRowCount() - 1; i >= 0; --i) {

			if (table.getModel().getValueAt(i, 0).equals(true)) {

				String id = (String) table.getModel().getValueAt(i, 1);
				int row = getRowByValue(id);
				((DefaultTableModel) table.getModel()).removeRow(row);
				dao.delete(dao.findById(id));

			}
		}

	}

	public int getRowByValue(Object id) {

		for (int i = table.getModel().getRowCount() - 1; i >= 0; --i) {

			if (table.getModel().getValueAt(i, 1).equals(id)) {

				return i;

			}
		}

		return -1;
	}

	public void saveRowsChanged() {

		for (String id : idsChanged) {

			System.out.println("Se busca el id: " + id);
			int row = getRowByValue(id);
			if (row != -1) {

				TableData object = (TableData) dao.findById((String) table.getModel().getValueAt(row, 1));
				try {

					object.setData(table.getModel(), row);
					dao.update(object);
				} catch (Exception e) {

					JOptionPane.showMessageDialog(null, "Formato Fecha Erroneo: yyyy-MM-dd", "Error BBDD",
							JOptionPane.ERROR_MESSAGE);
				}
			}

		}

		System.out.println("Se han actualizado: " + idsChanged.size());
		idsChanged = new ArrayList<String>();

	}

	public void updateTable(String id) {

		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		tableModel.setRowCount(0);
		List<TableData> object = dao.findAll();
		Object[] rowData = new Object[columnNames.length + 1];
		for (int i = 0; i < object.size(); i++) {
			
			if(id != null) {
				
				if(object.get(i).getID().equals(id)) {
					
					tableModel.addRow(object.get(i).getData());
					break;
				}
				
				
			}else {
				tableModel.addRow(object.get(i).getData());
			}
			
			
			
		}
		table.setModel(tableModel);

	}

	
}
