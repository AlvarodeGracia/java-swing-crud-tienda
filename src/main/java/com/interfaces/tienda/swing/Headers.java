package com.interfaces.tienda.swing;

public class Headers {

	public static String[] columnNamesTrabajador = {"Seleccionado","DNI", "nombre","Apellidos", "Nacimiento", "Telefono"};
	public static String[] columnNamesNomina = {"Seleccionado", "Id", "Trabajador", "Fecha", "Paga"};
	public static String[] columnNamesProveedor = {"Seleccionado", "Cif", "Direccion", "Telefon", "Nombre"};
	public static String[] columnNamesRecibo = {"Seleccionado", "Id", "Importe", "Precio Total"};
	public static String[] columnNamesStock = {"Seleccionado", "Id", "Producto", "Cantidad", "Vendidos"};
	public static String[] columnNamesPreoducto = {"Seleccionado", "Id", "Nombre", "Provedor", "Precio"};
	
}
