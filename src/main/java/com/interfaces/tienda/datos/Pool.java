package com.interfaces.tienda.datos;

import org.apache.commons.dbcp2.BasicDataSource;

public class Pool {
	
	private BasicDataSource basicDataSource;
	private static Pool instance = null;
	
	private Pool()
	{
		basicDataSource = new BasicDataSource();
		basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
		basicDataSource.setUsername("root");
		basicDataSource.setPassword("Patata!1");
		basicDataSource.setUrl("jdbc:mysql://localhost:3333/tienda?useSSL=false");
	}
	
	public static Pool getInstance()
	{
		if(instance == null)
			instance = new Pool();
		
		return instance;
	}

	public BasicDataSource getBasicDataSource() {
		return basicDataSource;
	}
	
	

}
