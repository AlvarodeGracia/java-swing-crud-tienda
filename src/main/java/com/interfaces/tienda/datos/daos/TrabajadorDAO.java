package com.interfaces.tienda.datos.daos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.dbcp2.BasicDataSource;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.Pool;
import com.interfaces.tienda.modelo.Trabajador;

public class TrabajadorDAO implements DAOInterface<Trabajador,String>{

	
	public Trabajador findById(String key) {
		
		Trabajador trabajador = null;
		
		try {
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			String query = "select * from Trabajadores where dni = ?" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
			s.setString(1,key);
			
			ResultSet rs = s.executeQuery ();
	        
			if (rs.first())
			{
				trabajador = new Trabajador();
				trabajador.setDni(rs.getString(1));
				trabajador.setNombre(rs.getString(2));
				trabajador.setApellidos(rs.getString(3));
				trabajador.setNacimiento(new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString(4)));
				trabajador.setTlf(rs.getString(5));

			}
			
			conexion.close();
			
		} catch (SQLException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return trabajador;
	}

	
	public List<Trabajador> findAll() {
		
		ArrayList<Trabajador> trabajadores =  new ArrayList();
		
		try{
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			Trabajador trabajador;
			
			String query = "select * from Trabajadores" ;
			PreparedStatement s = conexion.prepareStatement(query);
        
			ResultSet rs = s.executeQuery();
        
			// Se recorre el ResultSet, y se carga en un contenedor
			while (rs.next()){
				
				trabajador = new Trabajador();
				trabajador.setDni(rs.getString(1));
				trabajador.setNombre(rs.getString(2));
				trabajador.setApellidos(rs.getString(3));
				trabajador.setNacimiento(new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString(4)));
				trabajador.setTlf(rs.getString(5));
				trabajadores.add(trabajador);
			}
			// Se libera la conexion
	        conexion.close();
			
		}
		
		
    	catch (Exception ex){
    		System.out.println ("Error"+ex.getMessage());
    	}
		
		
		return trabajadores;
		
	}
	
	
	public int delete(Trabajador ob) {
		
		int cuantos = 0;
		try{
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			String query = "delete from Trabajadores where dni = ?" ;
			PreparedStatement s = conexion.prepareStatement(query);
			s.setString(1,ob.getDni());
			cuantos = s.executeUpdate ();
	        conexion.close();
	        
		}catch (Exception ex){
			
    		System.out.println ("Error"+ex.getMessage());
    		
    	}

		
		
		return cuantos;
	}

	
	public int insert(Trabajador ob) {
		
		int cuantos = 0;
		
		try{
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
				
			String query = "insert into Trabajadores values(?,?,?,?,?)" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
        
			s.setString(1,ob.getDni());
			s.setString(2, ob.getNombre());
			s.setString(3, ob.getApellidos());
			s.setDate(4, new Date(ob.getNacimiento().getTime()));
			s.setString (5, ob.getTlf());
        
			cuantos = s.executeUpdate ();
        
			// Se libera la conexion
	        conexion.close();
			
		}
		
		
    	catch (Exception ex)
    	{
    		System.out.println ("Error"+ex.getMessage());
    	}

		
		
		return cuantos;
	}

	
	public int update(Trabajador ob) {
		int cuantos = 0;
		
		try
		{
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
				
			String query = "update Trabajadores set dni = ?, nombre = ?, apellidos = ?, nacimiento = ?, telefono = ?"
					+ "where dni = ?" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
        
			s.setString(1,ob.getDni());
			s.setString(2, ob.getNombre());
			s.setString(3, ob.getApellidos());
			s.setDate(4, new Date(ob.getNacimiento().getTime()));
			s.setString (5, ob.getTlf());
        
			s.setString(6,ob.getDni());
        
			cuantos = s.executeUpdate ();
        
			
			// Se libera la conexion
	        conexion.close();
			
		}
		
		
    	catch (Exception ex)
    	{
    		System.out.println ("Error"+ex.getMessage());
    	}

		
		
		return cuantos;
	}

	

}
