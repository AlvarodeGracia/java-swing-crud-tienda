package com.interfaces.tienda.datos.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.Pool;
import com.interfaces.tienda.modelo.Nomina;
import com.interfaces.tienda.modelo.TableData;
import com.interfaces.tienda.modelo.Trabajador;

public class NominaDAO implements DAOInterface<Nomina, String>{

	public Nomina findById(String key) {
		
		Nomina nomina = null;
		try {
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			String query = "select * from Nomina where id_nomina = ?" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
	        
			s.setString(1,key);
        
			ResultSet rs = s.executeQuery ();
			
			if (rs.first()) {
				
				nomina = new Nomina();
				nomina.setId(rs.getString(1));
				TrabajadorDAO dao = new TrabajadorDAO();
				Trabajador trabajador = dao.findById(rs.getString(2));
				nomina.setTrabajador(trabajador);
				nomina.setFecha(rs.getDate(3));
				nomina.setPaga(rs.getFloat(4));
				
			}
			
			conexion.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return nomina;
		
	}

	
	public List<Nomina> findAll() {
		
		ArrayList<Nomina> nominas = new ArrayList<Nomina>();
		
		try
		{
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			Nomina nomina;
			
			String query = "select * from Nomina" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
			ResultSet rs = s.executeQuery ();
			
			while (rs.next())
			{
				nomina = new Nomina();
				nomina.setId(rs.getString(1));
				TrabajadorDAO dao = new TrabajadorDAO();
				Trabajador trabajador = dao.findById(rs.getString(2));
				nomina.setTrabajador(trabajador);
				nomina.setFecha(rs.getDate(3));
				nomina.setPaga(rs.getFloat(4));
				
				nominas.add(nomina);
			}
			
	        conexion.close();
			
		} catch (Exception e) {
    		e.printStackTrace();
    	}
		
		return nominas;
		
	}

	
	public int delete(Nomina ov) {
		
		int cuantos = 0;
		
		try
		{
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			String query = "delete * from Nomina where id_nomina = ?" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
        
			s.setString(1,ov.getId());
        
			cuantos = s.executeUpdate ();
			
	        conexion.close();
			
		}catch (Exception e) {
    		e.printStackTrace();
    	}
		
		return cuantos;
		
	}

	
	public int insert(Nomina ov) {
		
		int cuantos = 0;
		
		try
		{
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			String query = "insert into Nomina values(?,?,?,?)" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
        
			s.setString(1,ov.getId());
			s.setString(2, ov.getTrabajador().getDni());
			s.setDate (3, (java.sql.Date) new java.sql.Date(ov.getFecha().getTime()));
			s.setFloat(4, ov.getPaga());
			
			cuantos = s.executeUpdate ();
			
	        conexion.close();
			
		}catch (Exception e) {
    		e.printStackTrace();
    	}
		
		return cuantos;
		
	}

	
	public int update(Nomina ov) {
		
		int cuantos = 0;
		
		try
		{
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
				
			String query = "update Nomina set id_trabajador = ?,fecha = ?, paga = ? "
					+ "where id_nomina = ?";
			
			PreparedStatement s = conexion.prepareStatement(query);
        
			s.setString(1, ov.getTrabajador().getDni());
			s.setDate(2, (java.sql.Date) ov.getFecha());
			s.setFloat (3, ov.getPaga());
			s.setString (4, ov.getId());
			
			cuantos = s.executeUpdate ();
			
	        conexion.close();
			
		}catch (Exception e) {
    		e.printStackTrace();
    	}
		
		return cuantos;
		
	}

	
}
