package com.interfaces.tienda.datos.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.Pool;
import com.interfaces.tienda.modelo.Producto;
import com.interfaces.tienda.modelo.Proveedor;

public class ProductoDAO implements DAOInterface<Producto, String>{

	
	public Producto findById(String key) {
		
		Producto producto = null;
		try {
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			String query = "select * from Producto where id = ?" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
	        
			s.setString(1,key);
        
			ResultSet rs = s.executeQuery ();
			
			if (rs.first()) {
				
				producto = new Producto();
				producto.setId(rs.getString(1));
				producto.setNombre(rs.getString(2));
				producto.setPrecio(rs.getFloat(3));
				ProveedorDAO dao = new ProveedorDAO();
				Proveedor proveedor = dao.findById(rs.getString(4));
				producto.setProveedor(proveedor);
				
			}
			
			conexion.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return producto;
		
	}

	
	public List<Producto> findAll() {
		
		ArrayList<Producto> productos = new ArrayList<Producto>();
		
		try
		{
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			Producto producto;
			
			String query = "select * from Producto" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
			ResultSet rs = s.executeQuery ();
			
			while (rs.next()) {
				
				producto = new Producto();
				producto.setId(rs.getString(1));
				producto.setNombre(rs.getString(2));
				producto.setPrecio(rs.getFloat(3));
				ProveedorDAO dao = new ProveedorDAO();
				Proveedor proveedor = dao.findById(rs.getString(4));
				producto.setProveedor(proveedor);
				
				productos.add(producto);
			}
			
	        conexion.close();
			
		} catch (Exception e) {
    		e.printStackTrace();
    	}
		
		return productos;
		
	}

	
	public int delete(Producto ov) {
		
		int cuantos = 0;
		
		try
		{
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			String query = "delete * from Producto where id = ?" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
        
			s.setString(1,ov.getId());
        
			cuantos = s.executeUpdate ();
			
	        conexion.close();
			
		}catch (Exception e) {
    		e.printStackTrace();
    	}
		
		return cuantos;
		
	}

	
	public int insert(Producto ov) {
		
		int cuantos = 0;
		
		try {
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			String query = "insert into Producto values(?,?,?,?)" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
        
			s.setString(1,ov.getId());
			s.setString(2, ov.getNombre());
			s.setFloat(3, ov.getPrecio());
			s.setString (4, ov.getProveedor().getCif());
			
			cuantos = s.executeUpdate ();
			
	        conexion.close();
			
		}catch (Exception e) {
    		e.printStackTrace();
    	}
		
		return cuantos;
		
	}

	
	public int update(Producto ov) {
		
		int cuantos = 0;
		
		try {
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
				
			String query = "update Producto set nombre = ?,precio = ?, id_proveedor = ? "
					+ "where id = ?";
			
			PreparedStatement s = conexion.prepareStatement(query);
        
			s.setString(1, ov.getNombre());
			s.setFloat (2, ov.getPrecio());
			s.setString (3, ov.getProveedor().getCif());
			s.setString (4, ov.getId());
			
			cuantos = s.executeUpdate ();
			
	        conexion.close();
			
		}catch (Exception e) {
    		e.printStackTrace();
    	}
		
		return cuantos;
		
	}

	
}
