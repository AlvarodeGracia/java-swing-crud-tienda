package com.interfaces.tienda.datos.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.Pool;
import com.interfaces.tienda.modelo.Proveedor;

public class ProveedorDAO implements DAOInterface<Proveedor, String> {

	public Proveedor findById(String key) {
		
		Proveedor proveedor = null;
		try {
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			String query = "select * from Proveedor where cif = ?" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
	        
			s.setString(1,key);
        
			ResultSet rs = s.executeQuery ();
			
			if (rs.first()) {
				
				proveedor = new Proveedor();
				proveedor.setCif(rs.getString(1));
				proveedor.setDireccion(rs.getString(2));
				proveedor.setTlf(rs.getString(3));
				proveedor.setNombre(rs.getString(4));
				
			}
			
			conexion.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return proveedor;
		
	}

	public List<Proveedor> findAll() {
		
		ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();
		
		try
		{
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			Proveedor proveedor;
			
			String query = "select * from Proveedor" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
			ResultSet rs = s.executeQuery ();
			
			while (rs.next()) {
				
				proveedor = new Proveedor();
				proveedor.setCif(rs.getString(1));
				proveedor.setDireccion(rs.getString(2));
				proveedor.setTlf(rs.getString(3));
				proveedor.setNombre(rs.getString(4));
				
				proveedores.add(proveedor);
			}
			
	        conexion.close();
			
		} catch (Exception e) {
    		e.printStackTrace();
    	}
		
		return proveedores;
		
	}

	
	public int delete(Proveedor ov) {
		
		int cuantos = 0;
		
		try
		{
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			String query = "delete * from Proveedor where cif = ?" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
        
			s.setString(1,ov.getCif());
        
			cuantos = s.executeUpdate ();
			
	        conexion.close();
			
		}catch (Exception e) {
    		e.printStackTrace();
    	}
		
		return cuantos;
		
	}

	
	public int insert(Proveedor ov) {
		
		int cuantos = 0;
		
		try {
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			String query = "insert into Proveedor values(?,?,?,?)" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
        
			s.setString(1,ov.getCif());
			s.setString(2, ov.getDireccion());
			s.setString(3, ov.getTlf());
			s.setString (4, ov.getNombre());
			
			cuantos = s.executeUpdate ();
			
	        conexion.close();
			
		}catch (Exception e) {
    		e.printStackTrace();
    	}
		
		return cuantos;
		
	}

	
	public int update(Proveedor ov) {
		
		int cuantos = 0;
		
		try {
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
				
			String query = "update Proveedor set direccion = ?,tlf = ?, nombre = ? "
					+ "where cif = ?";
			
			PreparedStatement s = conexion.prepareStatement(query);
        
			s.setString(1, ov.getDireccion());
			s.setString (2, ov.getTlf());
			s.setString (3, ov.getNombre());
			s.setString (4, ov.getCif());
			
			cuantos = s.executeUpdate ();
			
	        conexion.close();
			
		}catch (Exception e) {
    		e.printStackTrace();
    	}
		
		return cuantos;
		
	}

	

}
