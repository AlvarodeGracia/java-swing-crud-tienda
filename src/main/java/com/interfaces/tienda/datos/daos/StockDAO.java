package com.interfaces.tienda.datos.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.Pool;
import com.interfaces.tienda.modelo.Producto;
import com.interfaces.tienda.modelo.Stock;

public class StockDAO implements DAOInterface<Stock, String>{

	
	public Stock findById(String key) {
		Stock object = null;
		
		try {
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			String query = "select * from Stock where id = ?" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
			s.setString(1,key);
			
			ResultSet rs = s.executeQuery ();
	        
			if (rs.first())
			{
				object = new Stock();
				object.setId(rs.getString(1));
				ProductoDAO dao = new ProductoDAO();
				Producto prod = dao.findById(rs.getString(2));
				object.setProducto(prod);
				object.setVendidos(rs.getInt(3));
				
			}
			
			conexion.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return object;
	}

	
	public List<Stock> findAll() {
		ArrayList<Stock> array =  new ArrayList();
		
		try{
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			Stock object;
			
			String query = "select * from Stock" ;
			PreparedStatement s = conexion.prepareStatement(query);
        
			ResultSet rs = s.executeQuery();
        
			// Se recorre el ResultSet, y se carga en un contenedor
			while (rs.next()){
				
				object = new Stock();
				object.setId(rs.getString(1));
				ProductoDAO dao = new ProductoDAO();
				Producto prod = dao.findById(rs.getString(2));
				object.setProducto(prod);
				object.setVendidos(rs.getInt(3));
				
				array.add(object);
			}
			// Se libera la conexion
	        conexion.close();
			
		}
		
		
    	catch (Exception ex){
    		System.out.println ("Error"+ex.getMessage());
    	}
		
		
		return array;
	}

	
	public int delete(Stock ob) {
		int cuantos = 0;
		try{
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			String query = "delete * from Stock where id = ?" ;
			PreparedStatement s = conexion.prepareStatement(query);
			s.setString(1,ob.getId());
			cuantos = s.executeUpdate ();
	        conexion.close();
	        
		}catch (Exception ex){
			
    		System.out.println ("Error"+ex.getMessage());
    		
    	}
		
		return cuantos;
		
	}

	
	public int insert(Stock ob) {
int cuantos = 0;
		
		try{
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
				
			String query = "insert into Stock values(?,?,?,?)" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
			
			s.setString(1, ob.getId());
			s.setString(2,ob.getProducto().getId());
			s.setInt(3, ob.getCantidad());
			s.setInt (4, ob.getVendidos());
        
			cuantos = s.executeUpdate ();
        
			// Se libera la conexion
	        conexion.close();
			
		}
		
		
    	catch (Exception ex)
    	{
    		System.out.println ("Error"+ex.getMessage());
    	}

		
		
		return cuantos;
	}

	
	public int update(Stock ob) {
int cuantos = 0;
		
		try
		{
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
				
			String query = "update Stock set id = ?, id_producto = ?, cantidad = ?, vendidos = ?"
					+ "where id = ?" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
        
			s.setString(1,ob.getId());
			s.setString(2, ob.getProducto().getId());
			s.setInt(3, ob.getCantidad());
			s.setInt(4, ob.getVendidos());
			
			s.setString(5,ob.getId());
			
			cuantos = s.executeUpdate ();
        
	        conexion.close();
			
		}
		
		
    	catch (Exception ex)
    	{
    		System.out.println ("Error"+ex.getMessage());
    	}

		
		
		return cuantos;
	}

	

}
