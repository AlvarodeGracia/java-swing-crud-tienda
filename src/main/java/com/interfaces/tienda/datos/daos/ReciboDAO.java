package com.interfaces.tienda.datos.daos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.Pool;
import com.interfaces.tienda.modelo.Recibo;

public class ReciboDAO implements DAOInterface<Recibo, String> {

	
	public Recibo findById(String key) {
		Recibo object = null;
		
		try {
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			String query = "select * from Recibos where id = ?" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
			s.setString(1,key);
			
			ResultSet rs = s.executeQuery ();
	        
			if (rs.first())
			{
				object = new Recibo();
				object.setId(rs.getString(1));
				object.setFecha(rs.getDate(2));
				object.setImporte(rs.getInt(3));
				object.setPrecio_total(rs.getInt(4));
				
			}
			
			conexion.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return object;
	}

	
	public List<Recibo> findAll() {
		ArrayList<Recibo> array =  new ArrayList();
		
		try{
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
			Recibo object;
			
			String query = "select * from Recibos" ;
			PreparedStatement s = conexion.prepareStatement(query);
        
			ResultSet rs = s.executeQuery();
        
			// Se recorre el ResultSet, y se carga en un contenedor
			while (rs.next()){
				
				object = new Recibo();
				object.setId(rs.getString(1));
				object.setFecha(rs.getDate(2));
				object.setImporte(rs.getInt(3));
				object.setPrecio_total(rs.getInt(4));
				
				array.add(object);
			}
			// Se libera la conexion
	        conexion.close();
			
		}
		
		
    	catch (Exception ex){
    		System.out.println ("Error"+ex.getMessage());
    	}
		
		
		return array;
	}

	
	public int delete(Recibo ob) {
		int cuantos = 0;
		try{
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			String query = "delete * from Recibos where id = ?" ;
			PreparedStatement s = conexion.prepareStatement(query);
			s.setString(1,ob.getId());
			cuantos = s.executeUpdate ();
	        conexion.close();
	        
		}catch (Exception ex){
			
    		System.out.println ("Error"+ex.getMessage());
    		
    	}
		
		return cuantos;
	}

	
	public int insert(Recibo ob) {
		int cuantos = 0;
		
		try{
			
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
				
			String query = "insert into Recibos values(?,?,?,?)" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
			
			s.setString(1, ob.getId());
			s.setDate(2,new Date(ob.getFecha().getTime()));
			s.setFloat(3, ob.getImporte());
			s.setFloat (4, ob.getPrecio_total());
        
			cuantos = s.executeUpdate();
        
	        conexion.close();
			
		}
		
		
    	catch (Exception ex)
    	{
    		System.out.println ("Error"+ex.getMessage());
    	}

		
		
		return cuantos;
	}

	
	public int update(Recibo ob) {
		int cuantos = 0;
		
		try
		{
			BasicDataSource basicDataSource = Pool.getInstance().getBasicDataSource();
			Connection conexion = basicDataSource.getConnection();
			
				
			String query = "update Stocks set id = ?, fecha = ?, importe = ?, precio_total = ?"
					+ "where id = ?" ;
			
			PreparedStatement s = conexion.prepareStatement(query);
        
			s.setString(1, ob.getId());
			s.setDate(2,new Date(ob.getFecha().getTime()));
			s.setFloat(3, ob.getImporte());
			s.setFloat (4, ob.getPrecio_total());
			
			s.setString(5,ob.getId());
			
			cuantos = s.executeUpdate ();
        
	        conexion.close();
			
		}
		
		
    	catch (Exception ex)
    	{
    		System.out.println ("Error"+ex.getMessage());
    	}

		
		
		return cuantos;
	}

}
