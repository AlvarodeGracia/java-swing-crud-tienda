package com.interfaces.tienda.datos;

import java.util.List;

public interface DAOInterface <T,K> {
	
	public T findById(K key);
	
	public  List<T> findAll();
	
	public int delete(T ob);
	
	public int insert(T ob);

	public int update(T ob);
	
}
