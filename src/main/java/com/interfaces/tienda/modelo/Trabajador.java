package com.interfaces.tienda.modelo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.table.TableModel;

import com.interfaces.tienda.swing.Headers;

public class Trabajador extends TableData{
	
	private String dni;
	private String nombre;
	private String apellidos;
	private Date nacimiento;
	private String tlf;
	
	public Trabajador(String dni, String nombre, String apellidos, Date nacimiento, String tlf) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.nacimiento = nacimiento;
		this.tlf = tlf;
		
	}
	
	public Trabajador() {
		this.dni = "";
		this.nombre = "";
		this.apellidos = "";
		this.nacimiento = null;
		this.tlf = "";
	}
	
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public Date getNacimiento() {
		return nacimiento;
	}
	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}
	public String getTlf() {
		return tlf;
	}
	public void setTlf(String tlf) {
		this.tlf = tlf;
	}

	@Override
	public String toString() {
		return "Trabajador [dni=" + dni + ", nombre=" + nombre + ", apellidos=" + apellidos + ", nacimiento="
				+ nacimiento + ", tlf=" + tlf + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trabajador other = (Trabajador) obj;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		return true;
	}

	@Override
	public Object[] getData() {
		
		Object[] obj = new Object[Headers.columnNamesTrabajador.length];
		
		obj[0] = false;
		obj[1] = getDni();
		obj[2] = getNombre();
		obj[3] = getApellidos();
		obj[4] = new SimpleDateFormat("yyyy-MM-dd").format(getNacimiento());
		obj[5] = getTlf();
		
		return obj;
		
	}

	@Override
	public void setData(TableModel model, int row) throws Exception {
	
		setDni((String)model.getValueAt(row, 1));
		setNombre((String)model.getValueAt(row, 2));
		setApellidos((String)model.getValueAt(row, 3));
		setNacimiento(new SimpleDateFormat("yyyy-MM-dd").parse((String) model.getValueAt(row, 4)));
		setTlf((String) model.getValueAt(row, 5));
		
	}

	@Override
	public String getID() {
		// TODO Auto-generated method stub
		return dni;
	}

	
	
}
