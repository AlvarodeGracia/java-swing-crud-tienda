package com.interfaces.tienda.modelo;

import java.util.Date;

import javax.swing.table.TableModel;

public class Recibo extends TableData{

	private String id;
	private Date fecha;
	private float importe;
	private float precio_total;
	
	public Recibo(String id, Date fecha, float importe, float precio_total) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.importe = importe;
		this.precio_total = precio_total;
	}
	
	public Recibo() {
		super();
		this.id = "";
		this.fecha = null;
		this.importe = 0;
		this.precio_total = 0;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public float getImporte() {
		return importe;
	}
	public void setImporte(float importe) {
		this.importe = importe;
	}
	public float getPrecio_total() {
		return precio_total;
	}
	public void setPrecio_total(float precio_total) {
		this.precio_total = precio_total;
	}

	@Override
	public String toString() {
		return "Recibo [id=" + id + ", fecha=" + fecha + ", importe=" + importe + ", precio_total=" + precio_total
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Recibo other = (Recibo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public Object[] getData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setData(TableModel model, int row) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getID() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
