package com.interfaces.tienda.modelo;

import java.text.SimpleDateFormat;

import javax.swing.table.TableModel;

import com.interfaces.tienda.swing.Headers;

public class Proveedor extends TableData{
	
	private String cif;
	private String direccion;
	private String tlf;
	private String nombre;
	
	public Proveedor(String cif, String direccion, String tlf, String nombre) {
		super();
		this.cif = cif;
		this.direccion = direccion;
		this.tlf = tlf;
		this.nombre = nombre;
	}
	
	public Proveedor() {
		super();
		this.cif = "";
		this.direccion = "";
		this.tlf = "";
		this.nombre = "";
	}

	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTlf() {
		return tlf;
	}

	public void setTlf(String tlf) {
		this.tlf = tlf;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Proveedor [cif=" + cif + ", direccion=" + direccion + ", tlf=" + tlf + ", nombre=" + nombre + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cif == null) ? 0 : cif.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Proveedor other = (Proveedor) obj;
		if (cif == null) {
			if (other.cif != null)
				return false;
		} else if (!cif.equals(other.cif))
			return false;
		return true;
	}

	@Override
	public Object[] getData() {
		Object[] obj = new Object[Headers.columnNamesProveedor.length];
		
		obj[0] = false;
		obj[1] = getCif();
		obj[2] = getDireccion();
		obj[3] = getTlf();
		obj[4] = getNombre();
		
		return obj;
		
	}

	@Override
	public void setData(TableModel model, int row) throws Exception {

		setCif((String)model.getValueAt(row, 1));
		setDireccion((String)model.getValueAt(row, 2));
		setTlf((String)model.getValueAt(row, 3));
		setNombre((String)model.getValueAt(row, 4));
		
		
	}

	@Override
	public String getID() {
		
		return cif;
	}
	
}
