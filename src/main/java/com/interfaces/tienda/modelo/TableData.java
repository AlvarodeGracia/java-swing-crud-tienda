package com.interfaces.tienda.modelo;

import javax.swing.table.TableModel;

public abstract class TableData {
	
	public abstract Object[] getData();
	public abstract void setData(TableModel model, int row) throws Exception;
	public abstract String getID();
	
}
