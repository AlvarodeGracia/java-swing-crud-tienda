package com.interfaces.tienda.modelo;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.table.TableModel;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.daos.TrabajadorDAO;
import com.interfaces.tienda.swing.Headers;

public class Nomina extends TableData {
	
	private String id;
	private Trabajador trabajador;
	private Date fecha;
	private float paga;
	
	public Nomina(String id, Trabajador trabajador, Date fecha, float paga) {
		super();
		this.id = id;
		this.trabajador = trabajador;
		this.fecha = fecha;
		this.paga = paga;
	}
	
	public Nomina() {
		super();
		this.id = "";
		this.trabajador = null;
		this.fecha = null;
		this.paga = 0;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Trabajador getTrabajador() {
		return trabajador;
	}

	public void setTrabajador(Trabajador trabajador) {
		this.trabajador = trabajador;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public float getPaga() {
		return paga;
	}

	public void setPaga(float paga) {
		this.paga = paga;
	}

	@Override
	public String toString() {
		return "Nomina [id=" + id + ", trabajador=" + trabajador + ", fecha=" + fecha + ", paga=" + paga + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Nomina other = (Nomina) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	

	@Override
	public Object[] getData() {
		
		Object[] obj = new Object[Headers.columnNamesNomina.length];
		
		obj[0] = false;
		obj[1] = getID();
		obj[2] = trabajador.getNombre()+"("+trabajador.getDni()+")";
		obj[3] = new SimpleDateFormat("yyyy-MM-dd").format(getFecha());
		obj[4] =  getPaga();
		
		return obj;
	}

	@Override
	public void setData(TableModel model, int row) throws Exception {
		
		DAOInterface dao = new TrabajadorDAO();
		
		setId((String)model.getValueAt(row, 1));
		setTrabajador((Trabajador)dao.findById((String)model.getValueAt(row, 2)));
		setFecha(new SimpleDateFormat("yyyy-MM-dd").parse((String) model.getValueAt(row, 3)));
		setPaga(Float.parseFloat((String)model.getValueAt(row, 5)));
		
	}

	@Override
	public String getID() {
		return id;
	}

	
	
	

}
