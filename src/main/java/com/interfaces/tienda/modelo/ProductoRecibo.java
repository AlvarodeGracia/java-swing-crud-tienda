package com.interfaces.tienda.modelo;

import javax.swing.table.TableModel;

public class ProductoRecibo extends TableData{

	private Recibo recibo;
	private Producto producto;
	private int cantidad;
	private float precio;
	
	public ProductoRecibo(Recibo recibo, Producto producto, int cantidad, float precio) {
		super();
		this.recibo = recibo;
		this.producto = producto;
		this.cantidad = cantidad;
		this.precio = precio;
	}
	
	public ProductoRecibo() {
		super();
		this.recibo = null;
		this.producto = null;
		this.cantidad = 0;
		this.precio = 0;
	}
	
	public Recibo getRecibo() {
		return recibo;
	}
	public void setRecibo(Recibo recibo) {
		this.recibo = recibo;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "ProductoRecibo [recibo=" + recibo + ", producto=" + producto + ", cantidad=" + cantidad + ", precio="
				+ precio + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((producto == null) ? 0 : producto.hashCode());
		result = prime * result + ((recibo == null) ? 0 : recibo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductoRecibo other = (ProductoRecibo) obj;
		if (producto == null) {
			if (other.producto != null)
				return false;
		} else if (!producto.equals(other.producto))
			return false;
		if (recibo == null) {
			if (other.recibo != null)
				return false;
		} else if (!recibo.equals(other.recibo))
			return false;
		return true;
	}

	@Override
	public Object[] getData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setData(TableModel model, int row) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getID() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
