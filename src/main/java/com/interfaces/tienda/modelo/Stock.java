package com.interfaces.tienda.modelo;

import java.text.SimpleDateFormat;

import javax.swing.table.TableModel;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.daos.ProductoDAO;
import com.interfaces.tienda.datos.daos.TrabajadorDAO;
import com.interfaces.tienda.swing.Headers;

public class Stock extends TableData{

	private String id;
	private Producto producto;
	private int cantidad;
	private int vendidos;
	
	public Stock(String id, Producto producto, int cantidad, int vendidos) {
		super();
		this.id = id;
		this.producto = producto;
		this.cantidad = cantidad;
		this.vendidos = vendidos;
	}
	
	public Stock() {
		super();
		this.id = "";
		this.producto = null;
		this.cantidad = 0;
		this.vendidos = 0;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public int getVendidos() {
		return vendidos;
	}
	public void setVendidos(int vendidos) {
		this.vendidos = vendidos;
	}

	@Override
	public String toString() {
		return "Stock [id=" + id + ", producto=" + producto + ", cantidad=" + cantidad + ", vendidos=" + vendidos + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stock other = (Stock) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public Object[] getData() {
		Object[] obj = new Object[Headers.columnNamesStock.length];
		
		obj[0] = false;
		obj[1] = getID();
		obj[2] = producto.getId();
		obj[3] = getCantidad();
		obj[4] = getVendidos();
		
		return obj;
	}

	@Override
	public void setData(TableModel model, int row) throws Exception {
		
		DAOInterface dao = new ProductoDAO();
		
		setId((String)model.getValueAt(row, 1));
		setProducto((Producto)dao.findById((String)model.getValueAt(row, 2)));
		setCantidad(Integer.parseInt((String)model.getValueAt(row, 3)));
		setVendidos(Integer.parseInt((String)model.getValueAt(row, 4)));
		
	}

	@Override
	public String getID() {
		
		return id;
	}

	
	
}
