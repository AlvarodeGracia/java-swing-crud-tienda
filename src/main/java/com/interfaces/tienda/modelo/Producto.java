package com.interfaces.tienda.modelo;

import java.text.SimpleDateFormat;

import javax.swing.table.TableModel;

import com.interfaces.tienda.datos.DAOInterface;
import com.interfaces.tienda.datos.daos.ProductoDAO;
import com.interfaces.tienda.datos.daos.ProveedorDAO;
import com.interfaces.tienda.datos.daos.TrabajadorDAO;
import com.interfaces.tienda.swing.Headers;

public class Producto extends TableData{
	
	private String id;
	private String nombre;
	private Proveedor proveedor;
	private float precio;
	
	public Producto(String id, String nombre, Proveedor proveedor, float precio) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.proveedor = proveedor;
		this.precio = precio;
	}
	
	public Producto() {
		super();
		this.id = "";
		this.nombre = "";
		this.proveedor = null;
		this.precio = 0;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "Producto [id=" + id + ", nombre=" + nombre + ", proveedor=" + proveedor + ", precio=" + precio + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Producto other = (Producto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public Object[] getData() {
		Object[] obj = new Object[Headers.columnNamesPreoducto.length];
		
		obj[0] = false;
		obj[1] = getID();
		obj[2] = getNombre();
		obj[3] = proveedor.getCif();
		obj[4] = getPrecio();
		
		return obj;
	}

	@Override
	public void setData(TableModel model, int row) throws Exception {

		DAOInterface dao = new ProveedorDAO();
		
		setId((String)model.getValueAt(row, 1));
		setNombre((String)model.getValueAt(row, 2));
		setProveedor((Proveedor)dao.findById((String)model.getValueAt(row, 3)));
		setPrecio(Float.parseFloat((String)model.getValueAt(row, 4)));
		
	}

	@Override
	public String getID() {
		return id;
	}
	
	

}
